/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.controllers;

import fashion.dao.CategoryDAO;
import fashion.dao.ProductDAO;
import fashion.dto.CategoryDTO;
import fashion.dto.ProductDTO;
import fashion.util.ActionConst;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Asus
 */
public class LoadProductController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ActionConst.ERROR;
        try {
            String action = request.getParameter("action");
            String page = request.getParameter("page");
            if (action.equals("loadProduct")) {
                if (page.equals("shop")) {
                    url = ActionConst.SHOP_PAGE;
                } else {
                    url = ActionConst.SUCCESS;
                }

            }
            if (action.equals("loadCategory")) {
                if (page.equals("cart")) {
                    url = ActionConst.CART_PAGE;
                }
                if (page.equals("checkout")) {
                    url = ActionConst.CHECKOUT_PAGE;
                }
                if (page.equals("contact")) {
                    url = ActionConst.CONTACT_PAGE;
                }
                if (page.equals("detail")) {
                    url = ActionConst.DETAIL_PAGE;
                }
                if (page.equals("shop")) {
                    url = ActionConst.SHOP_PAGE;
                }

            }

            if (action.equals("productManager")) {
                url = ActionConst.MANAGER_PRODUCT_PAGE;
            }

            ProductDAO proDAO = new ProductDAO();
            List<ProductDTO> listProduct = proDAO.getListProduct();
            CategoryDAO cateDAO = new CategoryDAO();
            List<CategoryDTO> listCategory = cateDAO.getListCategory();

            if (listProduct != null || listCategory != null) {
                request.setAttribute("LIST_PRODUCT", listProduct);
                request.setAttribute("LIST_CATEGORY", listCategory);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
