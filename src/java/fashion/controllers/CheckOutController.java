/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.controllers;

import fashion.dao.CartDAO;
import fashion.dao.OrderDAO;
import fashion.dao.OrderDetailDAO;
import fashion.dao.ShippingDAO;
import fashion.dto.OrderDTO;
import fashion.dto.OrderDetailDTO;
import fashion.dto.ProductDTO;
import fashion.dto.ShippingDTO;
import fashion.dto.UserDTO;
import fashion.util.ActionConst;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Asus
 */
public class CheckOutController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ActionConst.ERROR;
        try {
            UUID uuid = UUID.randomUUID();
            HttpSession session = request.getSession();

            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String address = request.getParameter("address");
            String country = request.getParameter("country");
            String region = request.getParameter("region");
            String city = request.getParameter("city");
            String orderId = uuid.toString();
            int zipCode = Integer.parseInt(request.getParameter("zipCode"));
            ShippingDAO shipDao = new ShippingDAO();

            boolean checkAddShipInfor = shipDao.addShippingInfor(new ShippingDTO(orderId, firstName, lastName, email, phone, address, country, region, city, zipCode));

            if (checkAddShipInfor) {

                UserDTO user = (UserDTO) session.getAttribute("USER_LOGIN");
                String userId = user.getUserId();
                String orderDay = new Date(System.currentTimeMillis()).toString();
                float total = Float.parseFloat(request.getParameter("total"));

                OrderDAO orderDao = new OrderDAO();
                boolean checkAddOrder = orderDao.addOrder(new OrderDTO(orderId, userId, orderDay, total));

                if (checkAddOrder) {
                    CartDAO shopCart = (CartDAO) session.getAttribute("CART");
                    Map<String, ProductDTO> cart = shopCart.getCart();
                    for (Map.Entry<String, ProductDTO> entry : cart.entrySet()) {
                        uuid = UUID.randomUUID();
                        String detailId = uuid.toString();
                        String productId = entry.getValue().getProductId();
                        double price = entry.getValue().getPrice();
                        int quantity = entry.getValue().getQuantity();
                        String color = entry.getValue().getColor();
                        String size = entry.getValue().getSize();
                        OrderDetailDAO detailDao = new OrderDetailDAO();
                        boolean checkAddDetail = detailDao.addOrderDetail(new OrderDetailDTO(detailId, productId, orderId, price, quantity, size, color));
                        if (checkAddDetail) {
                            url = ActionConst.CHECKOUT_PAGE;
                        }
                    }
                    session.removeAttribute("CART");
                    cart.clear();
                }

            }

        } catch (NumberFormatException | SQLException e) {
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
