/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.controllers;

import fashion.util.ActionConst;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Asus
 */
public class MainController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = "";
        try {
            String action = request.getParameter("action");
            if (action.equals("login")) {
                url = ActionConst.LOGIN;
            } else if (action.equals("logOut")) {
                url = ActionConst.LOGOUT;
            } else if (action.equals("loadProduct")) {
                url = ActionConst.LOAD_PRODUCT;
            } else if (action.equals("loadCategory")) {
                url = ActionConst.LOAD_PRODUCT;
            } else if (action.equals("loadDetail")) {
                url = ActionConst.LOAD_DETAIL;
            } else if (action.equals("addToCart")) {
                url = ActionConst.ADD_TO_CART;
            } else if (action.equals("loadCustomer")) {
                url = ActionConst.LOAD_USER;
            } else if (action.equals("addUser")) {
                url = ActionConst.ADD_USER;
            } else if (action.equals("deleteUser")) {
                url = ActionConst.DELETE_USER;
            } else if (action.equals("edit-user")) {
                url = ActionConst.EDIT_USER;
            } else if (action.equals("plus-quantity")) {
                url = ActionConst.SET_QUANTITY;
            } else if (action.equals("minus-quantity")) {
                url = ActionConst.SET_QUANTITY;
            }else if (action.equals("remove-item")) {
                url = ActionConst.REMOVE_ITEM;
            } else if (action.equals("productManager")) {
                url = ActionConst.LOAD_PRODUCT;
            } else if (action.equals("addProduct")) {
                url = ActionConst.ADD_PRODUCT;
            } else if (action.equals("deleteProduct")) {
                url = ActionConst.DELETE_PRODUCT;
            } else if (action.equals("edit-product")) {
                url = ActionConst.EDIT_PRODUCT;
            }else if (action.equals("checkout")) {
                url = ActionConst.CHECKOUT;
            }else if (action.equals("register")) {
                url = ActionConst.SIGN_UP;
            }else if (action.equals("search")) {
                url = ActionConst.SEARCH;
            }


        } catch (Exception e) {
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
