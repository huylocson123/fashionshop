/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.controllers;

import fashion.dao.CartDAO;
import fashion.dto.GoogleDTO;
import fashion.dto.ProductDTO;
import fashion.dto.UserDTO;
import fashion.util.ActionConst;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Asus
 */
public class AddToCartController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ActionConst.ERROR;
        try {
            HttpSession session = request.getSession();
            UserDTO user = (UserDTO) session.getAttribute("USER_LOGIN");
            GoogleDTO googleDto = (GoogleDTO) session.getAttribute("GOOGLE_USER_LOGIN");
            if (user == null && googleDto == null) {
                url = ActionConst.LOGIN_PAGE;
            } else {
                String productID = request.getParameter("proId");
                String productName = request.getParameter("name");
                double productPrice = Float.parseFloat(request.getParameter("price"));
                String image = request.getParameter("img");
                String color = request.getParameter("color");
                String size = request.getParameter("size");
                int quantity = Integer.parseInt(request.getParameter("quantity"));

                if (quantity <= 0) {
                    request.setAttribute("ERROR", "Choose valid quantity");
                    url = ActionConst.DETAIL_PAGE;

                } else {
                    CartDAO cart = (CartDAO) session.getAttribute("CART");

                    if (cart == null) {
                        cart = new CartDAO();
                    }

                    ProductDTO proDuct = new ProductDTO(productID, productName, productPrice, image, quantity, size, color);
                    boolean check = cart.addToCart(proDuct);
                    if (check) {
                        session.setAttribute("CART", cart);
                        url = ActionConst.ADD_SUCCESS;
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
