/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.util;

/**
 *
 * @author Asus
 */
public class ActionConst {

//    Login
    public static String LOGIN = "LoginController";
    public static final String LOGIN_PAGE = "login.jsp";
    public static final String ADMIN_PAGE = "admin.jsp";

//    Logout
    public static final String LOGOUT = "LogOutController";

//    Role
    public static final String ADMIN = "2";
    public static final String USER = "1";

//    Status
    public static final String LOGIN_SUCCESS = "index.jsp";
    public static final String LOGIN_FAIL = "login.jsp";
    public static final String SUCCESS = "index.jsp";
    public static final String ERROR = "error.jsp";

//    Product
    public static final String LOAD_PRODUCT = "LoadProductController";
    public static final String MANAGER_PRODUCT_PAGE = "product.jsp";
    public static final String ADD_PRODUCT = "AddProductController";
    public static final String DELETE_PRODUCT = "DeleteProductController";
    public static final String EDIT_PRODUCT = "EditProductController";
    public static final String SET_QUANTITY = "SetQuantityController";
    public static final String SEARCH = "SearchProductController";

//    Category
    public static final String LOAD_CATEGORY = "LoadCategoryController";
    public static final String HEADER_JSP = "header.jsp";

//    Cart 
    public static final String ADD_TO_CART = "AddToCartController";
    public static final String ADD_SUCCESS = "cart.jsp";
    public static final String REMOVE_ITEM = "RemoveCartItemController";
    public static final String CART_PAGE = "cart.jsp";

//    Detail
    public static final String LOAD_DETAIL = "LoadDetailController";
    public static final String DETAIL_PAGE = "detail.jsp";

//    User
    public static final String LOAD_USER = "LoadCustomerController";
    public static final String MANAGER_USER_PAGE = "user.jsp";
    public static final String ADD_USER = "AddUserController";
    public static final String DELETE_USER = "DeleteUserController";
    public static final String EDIT_USER = "EditUserController";

//    Check out 
    public static final String CHECKOUT = "CheckOutController";
    public static final String CHECKOUT_PAGE = "checkout.jsp";

//    Sign up
    public static final String SIGN_UP = "SignUpController";

//    Contact
    public static final String CONTACT_PAGE = "contact.jsp";

//    Shop 
    public static final String SHOP_PAGE = "shop.jsp";

}
