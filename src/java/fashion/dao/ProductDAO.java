/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.ProductDTO;
import fashion.dto.UserDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class ProductDAO {

    public List<ProductDTO> getListProduct() throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        List<ProductDTO> listProduct = new ArrayList<>();
        try {
            conn = DButil.getConnection();
            String query = "SELECT * FROM Product ";
            stm = conn.prepareStatement(query);
            rs = stm.executeQuery();

            while (rs.next()) {
                String proId = rs.getString("Id");
                String proName = rs.getString("Name");
                String cateId = rs.getString("CateId");
                String img = rs.getString("Img");
                int quantity = rs.getInt("Quantity");
                String proDesc = rs.getString("Description");
                float price = rs.getFloat("Price");
                String createDate = rs.getString("CreateDate");
                boolean status = rs.getBoolean("Status");

                listProduct.add(new ProductDTO(proId, proName, cateId, img, quantity, proDesc, price, createDate, status));

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return listProduct;
    }

    public List<ProductDTO> getProductById(String productId) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        List<ProductDTO> listProduct = new ArrayList<>();
        try {
            conn = DButil.getConnection();
            String query = "SELECT * FROM Product "
                    + " WHERE Id = ?";
            stm = conn.prepareStatement(query);
            stm.setString(1, productId);
            rs = stm.executeQuery();

            while (rs.next()) {
                String proName = rs.getString("Name");
                String cateId = rs.getString("CateId");
                String img = rs.getString("Img");
                int quantity = rs.getInt("Quantity");
                String proDesc = rs.getString("Description");
                double price = rs.getDouble("Price");
                String createDate = rs.getString("CreateDate");
                boolean status = rs.getBoolean("Status");

                listProduct.add(new ProductDTO(productId, proName, cateId, img, quantity, proDesc, price, createDate, status));

            }

        } catch (ClassNotFoundException | SQLException e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return listProduct;
    }

    public boolean addProduct(ProductDTO pro) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DButil.getConnection();
            String query = "INSERT INTO Product "
                    + " VALUES (?,?,?,?,?,?,?,?,?)";
            stm = conn.prepareStatement(query);
            stm.setString(1, pro.getProductId());
            stm.setString(2, pro.getProductName());
            stm.setString(3, pro.getCategoryId());
            stm.setString(4, pro.getImage());
            stm.setInt(5, pro.getQuantity());
            stm.setString(6, pro.getProDesc());
            stm.setDouble(7, pro.getPrice());
            stm.setString(8, pro.getCreateDate());
            stm.setInt(9, 1);

            stm.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }

    public boolean deleteProduct(String proId) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DButil.getConnection();
            String query = " UPDATE Product "
                    + " SET [Status] = 0"
                    + " WHERE Product.Id = ? ";

            stm = conn.prepareStatement(query);
            stm.setString(1, proId);

            rs = stm.executeQuery();
        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }

    public boolean editProduct(ProductDTO pro) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DButil.getConnection();
            String query = "UPDATE [Product] "
                    + " SET  [Product].[Name] = ? , [Product].[CateId] = ? , [Product].Img = ? , [Product].[Quantity] = ? ,[Product].[Description] = ?, [Product].[Price] = ?, [Product].CreateDate = ? , [Product].[status] = ?  "
                    + " WHERE [Product].Id = ?";

            stm = conn.prepareStatement(query);
            stm.setString(1, pro.getProductName());
            stm.setString(2, pro.getCategoryId());
            stm.setString(3, pro.getImage());
            stm.setInt(4, pro.getQuantity());
            stm.setString(5, pro.getProDesc());
            stm.setDouble(6, pro.getPrice());
            stm.setString(7, pro.getCreateDate());
            boolean status = pro.isProStatus();
            stm.setBoolean(8, pro.isProStatus());
            stm.setString(9, pro.getProductId());
            rs = stm.executeQuery();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }

    public List<ProductDTO> searchProduct(String text) throws SQLException {

        List<ProductDTO> listProduct = new ArrayList<>();
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DButil.getConnection();
            if (conn != null) {
                String sql = " SELECT Id ,Name, CateId, Img, Quantity, Description ,Price, CreateDate, Status"
                        + " FROM Product " + " WHERE Name like ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + text + "%");
                rs = stm.executeQuery();
                while (rs.next()) {
                    String productID = rs.getString("Id");
                    String name = rs.getString("Name");
                    String cateId = rs.getString("CateId");
                    String img = rs.getString("Img");
                    int quantity = rs.getInt("Quantity");
                    String description = rs.getString("Description");
                    double price = rs.getDouble("Price");
                    String createDay = rs.getString("CreateDate");
                    boolean status = rs.getBoolean("Status");
                    listProduct.add(new ProductDTO(productID, name, cateId, img, quantity, description, price, createDay, status));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return listProduct;

    }

}
