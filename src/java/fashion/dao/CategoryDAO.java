/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.CategoryDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class CategoryDAO {

    public List<CategoryDTO> getListCategory() throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        List<CategoryDTO> listCategory = new ArrayList<>();
        try {
            conn = DButil.getConnection();
            String query = "SELECT * FROM Category ";
            stm = conn.prepareStatement(query);
            rs = stm.executeQuery();

            while (rs.next()) {
                String cateId = rs.getString("Id");
                String cateName = rs.getString("Name");
                String cateImg = rs.getString("Img");
                listCategory.add(new CategoryDTO(cateId, cateName, cateImg));

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return listCategory;
    }
}
