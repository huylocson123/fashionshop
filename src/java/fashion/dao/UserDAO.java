/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.CustomerDTO;
import fashion.dto.UserDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class UserDAO {

    public UserDTO checkLogin(String email, String password) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        UserDTO user = null;
        try {
            conn = DButil.getConnection();
            String query = "SELECT * FROM [User] "
                    + " WHERE Email = ? AND [Password] = ?";
            stm = conn.prepareStatement(query);
            stm.setString(1, email);
            stm.setString(2, password);
            rs = stm.executeQuery();

            if (rs.next()) {
                String userId = rs.getString("Id");
                String roleId = rs.getString("RoleId");
                boolean status = rs.getBoolean("Status");
                user = new UserDTO(userId, email, password, roleId, status);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return user;

    }

    public boolean addNewUser(CustomerDTO customer) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DButil.getConnection();
            String query = " INSERT INTO [User] (Id , Email , [Password] , RoleId , [Status])"
                    + " VALUES (?,?,?,?,?)"
                    + " INSERT INTO [Customer] (Id , [Address] , CreateDay , PhoneNumber, FirstName, LastName)"
                    + " VALUES (?,?,?,?,?,?)";
            stm = conn.prepareStatement(query);
            String userId = customer.getCustomerId();
            String email = customer.getEmail();
            String password = customer.getPassword();
            String roleId = customer.getRoleId();
            int status = 1;
            String address = customer.getAddress();
            String createDay = customer.getCreateDay();
            String phone = customer.getPhoneNumber();
            String firstName = customer.getFirstName();
            String lastName = customer.getLastName();
            stm.setString(1, userId);
            stm.setString(2, email);
            stm.setString(3, password);
            stm.setString(4, roleId);
            stm.setInt(5, status);
            stm.setString(6, userId);
            stm.setString(7, address);
            stm.setString(8, createDay);
            stm.setString(9, phone);
            stm.setString(10, firstName);
            stm.setString(11, lastName);

            rs = stm.executeQuery();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }

    public boolean deleteUser(String userId) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DButil.getConnection();
            String query = " UPDATE [User] "
                    + " SET [Status] = 0"
                    + " WHERE [User].Id = ? ";

            stm = conn.prepareStatement(query);
            stm.setString(1, userId);

            rs = stm.executeQuery();
        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return true;
    }

    public boolean asign(UserDTO user) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {

            conn = DButil.getConnection();
            String query = "INSERT INTO [User] (Id, Email, Password, RoleId, Status)"
                    + " VALUES (?,?,?,?,?)";
            stm = conn.prepareStatement(query);
            String id = user.getUserId();
            String email = user.getEmail();
            String password = user.getPassword();
            String role = user.getRoleId();

            stm.setString(1, id);
            stm.setString(2, email);
            stm.setString(3, password);
            stm.setString(4, role);
            stm.setInt(5, 1);

            stm.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return true;
    }

}
