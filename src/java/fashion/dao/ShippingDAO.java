/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fashion.dao;

import fashion.dto.ShippingDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author huylocson123
 */
public class ShippingDAO {

    public boolean addShippingInfor(ShippingDTO ship) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DButil.getConnection();
            if (conn != null) {

                String sql = " INSERT INTO [Shipping] (Id, FirstName, LastName , Email , PhoneNumber, [Address], Country, Region, City, ZipCode)  "
                        + " VALUES (?,?,?,?,?,?,?,?,?,?) ";
                stm = conn.prepareStatement(sql);
                String id = ship.getShippingId();
                String firstName = ship.getFirstName();
                String lastName = ship.getLastName();
                String email = ship.getEmail();
                String phone = ship.getPhone();
                String address = ship.getAddress();
                String country = ship.getCountry();
                String region = ship.getRegion();
                String city = ship.getCity();
                int zipCode = ship.getZipCode();
                stm.setString(1, id);
                stm.setString(2, firstName);
                stm.setString(3, lastName);
                stm.setString(4, email);
                stm.setString(5, phone);
                stm.setString(6, address);
                stm.setString(7, country);
                stm.setString(8, region);
                stm.setString(9, city);
                stm.setInt(10, zipCode);

                rs = stm.executeQuery();

            }

        } catch (ClassNotFoundException | SQLException e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }

}
