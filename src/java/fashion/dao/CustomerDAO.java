/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.CustomerDTO;
import fashion.dto.UserDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class CustomerDAO {

    public List<CustomerDTO> getAllUser() throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        UserDTO user = null;
        List<CustomerDTO> listCustomer = new ArrayList<>();
        try {
            conn = DButil.getConnection();
            String query = "SELECT [User].Id, [Address], CreateDay , PhoneNumber , FirstName , LastName , Email , [Password] ,RoleId ,[Status] FROM [Customer] "
                    + " INNER JOIN [User] ON Customer.Id = [User].Id";
            stm = conn.prepareStatement(query);
            rs = stm.executeQuery();
            while (rs.next()) {
                String userId = rs.getString("Id");
                String address = rs.getString("Address");
                String createDay = rs.getString("CreateDay");
                String phoneNumber = rs.getString("PhoneNumber");
                String firstName = rs.getString("FirstName");
                String lastName = rs.getString("LastName");
                String email = rs.getString("Email");
                String password = rs.getString("Password");
                String roleId = rs.getString("RoleId");
                boolean status = rs.getBoolean("Status");
                listCustomer.add(new CustomerDTO(userId, address, createDay, phoneNumber, firstName, lastName, email, password, roleId, status));
            }

        } catch (Exception e) {
        } finally {

            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return listCustomer;
    }

    public boolean update(CustomerDTO customer) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DButil.getConnection();
            String query = "UPDATE [User] "
                    + "SET  [User].Email = ? , [User].[Password] = ? , [User].RoleId = ? , [User].[Status] = ? "
                    + "WHERE [User].Id = ? "
                    + "UPDATE [Customer] "
                    + "SET [Customer].[Address] =? , [Customer].CreateDay = ? , [Customer].PhoneNumber = ? , [Customer].FirstName = ?, [Customer].LastName = ? "
                    + "WHERE [Customer].Id = ?";
            String address = customer.getAddress();
            String createDay = customer.getCreateDay();
            String phone = customer.getPhoneNumber();
            String firstName = customer.getFirstName();
            String lastName = customer.getLastName();
            String email = customer.getEmail();
            String password = customer.getPassword();
            String role = customer.getRoleId();
            boolean status = customer.isStatus();
            String id = customer.getCustomerId();
            stm = conn.prepareStatement(query);
            
            stm.setString(1, email);
            stm.setString(2, password);
            stm.setString(3, role);
            stm.setBoolean(4, status);          
            stm.setString(5, id);
            
            stm.setString(6, address);
            stm.setString(7, createDay);
            stm.setString(8, phone);
            stm.setString(9, firstName);
            stm.setString(10, lastName);
            stm.setString(11, id);
            rs = stm.executeQuery();

        } catch (ClassNotFoundException | SQLException e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return true;
    }
}
