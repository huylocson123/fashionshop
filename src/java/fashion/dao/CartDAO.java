/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.ProductDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Asus
 */
public class CartDAO {

    private Map<String, ProductDTO> cart;

    public CartDAO() {
        cart = new HashMap<>();
    }

    public CartDAO(Map<String, ProductDTO> cart) {
        this.cart = cart;
    }

    public Map<String, ProductDTO> getCart() {
        return cart;
    }

    public void setCart(Map<String, ProductDTO> cart) {
        this.cart = cart;
    }

    public boolean addToCart(ProductDTO product) {
        boolean check = false;
        String productId = product.getProductId();
        String color = product.getColor();
        String size = product.getSize();
        String key = productId + color + size;
        try {
            if (this.cart == null) {
                this.cart = new HashMap<>();
            }

            if (this.cart.containsKey(key)) {
                int currentQuantity = this.cart.get(key).getQuantity();
                product.setQuantity(currentQuantity + product.getQuantity());
            }

            this.cart.put(key, product);
            check = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return check;
    }

    public boolean setQuantity(String Id, String color, String size, int quantity, int step) {
        String key = Id + color + size;
        try {
            if (this.cart.containsKey(key)) {
                int currentQuantity = this.cart.get(key).getQuantity();
                if (currentQuantity + step == 0) {
                    this.cart.remove(key);
                }

                this.cart.get(key).setQuantity(currentQuantity + step);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public boolean removeItem(String key) {
        try {
            if (this.cart.get(key) != null) {
                this.cart.remove(key);
            }
        } catch (Exception e) {
        }
        return true;
    }

}
