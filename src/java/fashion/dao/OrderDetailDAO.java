/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fashion.dao;

import fashion.dto.OrderDetailDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author huylocson123
 */
public class OrderDetailDAO {

    public boolean addOrderDetail(OrderDetailDTO orderDetail) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DButil.getConnection();
            if (conn != null) {
                String sql = " INSERT INTO [OrderDetail] (Id , ProductID , OrderId , Price , Quantity , Size , Color)  "
                        + " VALUES (?,?,?,?,?,?,?) ";
                stm = conn.prepareStatement(sql);
                String orderDetailId = orderDetail.getDetailId();
                String productId = orderDetail.getProductId();
                String orderId = orderDetail.getOrderId();
                double price = orderDetail.getPrice();
                int quantity = orderDetail.getQuantity();
                String size = orderDetail.getSize();
                String color = orderDetail.getColor();

                stm.setString(1, orderDetailId);
                stm.setString(2, productId);
                stm.setString(3, orderId);
                stm.setDouble(4, price);
                stm.setInt(5, quantity);
                stm.setString(6, size);
                stm.setString(7, color);

                stm.executeUpdate();

            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }

}
