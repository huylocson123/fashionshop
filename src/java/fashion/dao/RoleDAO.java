/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.RoleDTO;
import fashion.util.DButil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class RoleDAO {

    public List<RoleDTO> getRole() throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        List<RoleDTO> listRole = new ArrayList<>();
        try {
            conn = DButil.getConnection();
            String query = "SELECT * FROM Role ";
            stm = conn.prepareStatement(query);
            rs = stm.executeQuery();

            while (rs.next()) {
                String roleId = rs.getString("Id");
                String roleName = rs.getString("Name");
                listRole.add(new RoleDTO(roleId, roleName));

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return listRole;
    }
}
