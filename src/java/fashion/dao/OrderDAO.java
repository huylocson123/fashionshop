/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dao;

import fashion.dto.OrderDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import fashion.util.DButil;
import java.sql.Date;
import java.sql.SQLException;

/**
 *
 * @author Asus
 */
public class OrderDAO {

    public boolean addOrder(OrderDTO order) throws SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DButil.getConnection();
            if (conn != null) {
                String sql = " INSERT INTO [Order] (Id , UserId , OrderDate , Total)  "
                        + " VALUES (?,?,?,?) ";
                stm = conn.prepareStatement(sql);
                String orderId = order.getOrderId();
                String userId = order.getUserId();
                String orderDate = order.getOrderDate();
                float total = order.getTotal();
                stm.setString(1, orderId);
                stm.setString(2, userId);
                stm.setString(3, orderDate);
                stm.setFloat(4, total);
                rs = stm.executeQuery();

            }

        } catch (ClassNotFoundException | SQLException e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return true;
    }
}
