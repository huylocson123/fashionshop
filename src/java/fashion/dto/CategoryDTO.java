/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dto;

/**
 *
 * @author Asus
 */
public class CategoryDTO {
    private String cateId;
    private String cateName;
    private String cateImg;

    public CategoryDTO() {
    }

    public CategoryDTO(String cateId, String cateName, String cateImg) {
        this.cateId = cateId;
        this.cateName = cateName;
        this.cateImg = cateImg;
    }

    public String getCateId() {
        return cateId;
    }

    public void setCateId(String cateId) {
        this.cateId = cateId;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public String getCateImg() {
        return cateImg;
    }

    public void setCateImg(String cateImg) {
        this.cateImg = cateImg;
    }
    
}
