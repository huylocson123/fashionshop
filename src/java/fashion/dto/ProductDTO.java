/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dto;

/**
 *
 * @author Asus
 */
public class ProductDTO {

    private String productId;
    private String productName;
    private String categoryId;
    private String image;
    private int quantity;
    private String proDesc;
    private double price;
    private String createDate;
    private boolean proStatus;
    private String size;
    private String color;

    public ProductDTO() {
    }

    public ProductDTO(String productId, String productName, String categoryId, String image, int quantity, String proDesc, double price, String createDate, boolean proStatus, String size, String color) {
        this.productId = productId;
        this.productName = productName;
        this.categoryId = categoryId;
        this.image = image;
        this.quantity = quantity;
        this.proDesc = proDesc;
        this.price = price;
        this.createDate = createDate;
        this.proStatus = proStatus;
        this.size = size;
        this.color = color;
    }

    public ProductDTO(String productID, String productName, double productPrice, String image, int quantity, String size, String color) {
        this.productId = productID;
        this.productName = productName;
        this.price = productPrice;
        this.image = image;
        this.quantity = quantity;
        this.size = size;
        this.color = color;

    }

    public ProductDTO(String proId, String name, String category, String img, int quantity, String description, double price, String createDay, boolean status) {
        this.productId = proId;
        this.productName = name;
        this.categoryId = category;
        this.image = img;
        this.quantity = quantity;
        this.proDesc = description;
        this.price = price;
        this.createDate = createDay;
        this.proStatus = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProDesc() {
        return proDesc;
    }

    public void setProDesc(String proDesc) {
        this.proDesc = proDesc;
    }

    public double getPrice() {
        return Math.round(price * 100.0)/100.0 ;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public boolean isProStatus() {
        return proStatus;
    }

    public void setProStatus(boolean proStatus) {
        this.proStatus = proStatus;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
