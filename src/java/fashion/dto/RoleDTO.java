/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fashion.dto;

/**
 *
 * @author Asus
 */
public class RoleDTO {

    private String roelId;
    private String roleName;

    public RoleDTO() {
    }

    public RoleDTO(String roelId, String roleName) {
        this.roelId = roelId;
        this.roleName = roleName;
    }

    public String getRoelId() {
        return roelId;
    }

    public void setRoelId(String roelId) {
        this.roelId = roelId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
