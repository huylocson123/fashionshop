<%-- 
    Document   : login
    Created on : Jan 7, 2023, 2:23:46 PM
    Author     : Asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
              integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/Login/assets/css/login.css">


    </head>
    <body>
        <div class="wrapper">
            <div class="content">

                <div class="content-left">

                    <div class="logo">
                        <img src="" alt=""
                             srcset="https://cdn.dribbble.com/users/5017113/avatars/normal/3346034d0a9409f111aaa33e2ad6cd51.png?1664186864">
                        <span>Fashion</span>
                    </div>

                    <div class="content-form">
                        <div class="title">
                            <h1>Welcome back</h1>
                            <p>Please enter your contact to connect</p>
                        </div>
                        <form action="MainController" method="POST" class="contact-form">
                            <div class="signin-form">
                                <span>Email address</span>
                                <input id="noSpacesField" type="text" name="email"  placeholder="name@gmail.com" onblur="this.value = removeSpaces(this.value);">
                                <span>Password</span>
                                <input id="noSpacesField"type="password" name="password"  placeholder="*******"onblur="this.value = removeSpaces(this.value);">      
                                <c:if test="${requestScope.ERROR !=null}">
                                    <c:set var="emailError" value="${requestScope.ERROR}"></c:set>
                                    <span class="error red" style="color:red;">${emailError}</span>
                                </c:if>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" aria-label="checkbox" class="">
                                <span>Remember me</span>
                                <a href="">Forgot password</a>
                            </div>
                            <button type="submit" class="btn-login"  name="action" value="login">Log in</button>

                            
                            <a class="btn-google button" href="https://accounts.google.com/o/oauth2/auth?scope=profile&redirect_uri=http://localhost:8080/FashionShop/LoginGoogleController&response_type=code&client_id=44010883311-4sps2q9hhirbni1gda3rqkcqr87di448.apps.googleusercontent.com&approval_prompt=force">
                                    <i class="fab fa-google"></i>
                                    Log in with google
                                </a>  
                            

                            <div class="signin-link">
                                <span>Don't have account ? <a href="register.jsp">Sign up here</a></span>
                            </div>

                        </form>
                    </div>

                </div>

                <div class="content-right">
                    <img src="./Login/assets/img/hand-painted-watercolor-pastel-sky-background_23-2148902771 (1).webp" alt="">
                </div>
            </div>
        </div>
        <script language="javascript" type="text/javascript">
            function removeSpaces(string) {
                return string.split(' ').join('');
            }
        </script>
    </body>
</html>
