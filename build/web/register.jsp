<%-- 
    Document   : login
    Created on : Jan 7, 2023, 2:23:46 PM
    Author     : Asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
              integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="./Login/assets/css/style.css">

    </head>
    <body>
        <div class="wrapper">
            <div class="content">
                
                <div class="content-left">
                    
                    <div class="logo">
                        <img src="" alt=""
                             srcset="https://cdn.dribbble.com/users/5017113/avatars/normal/3346034d0a9409f111aaa33e2ad6cd51.png?1664186864">
                        <span>Fashion</span>
                    </div>
                 
                    <div class="content-form">
                        <div class="title">
                            <h1>Welcome</h1>
                            <p>Become our new member !</p>
                        </div>
                        <form action="MainController" method="POST" class="contact-form">
                            <div class="signin-form">
                                <span>Email address</span>
                                <input type="text" name="email"  placeholder="name@gmail.com">
                                <span>Password</span>
                                <input type="password" name="password"  placeholder="*******">
                                <span>Confirm password</span>
                                <input type="password" name="password-confirm"  placeholder="*******">
                              
                            </div>
                         
                            <button type="submit" class="btn-login"  name="action" value="register">Assign</button>                 
                            <button type="" class="btn-google">
                                <i class="fab fa-google"></i>
                                Log in with google</button>
                            
                        </form>
                    </div>
                    
                </div>
                
                <div class="content-right">
                    <img src="./Login/assets/img/hand-painted-watercolor-pastel-sky-background_23-2148902771 (1).webp" alt="">
                </div>
            </div>
        </div>
    </body>
</html>
