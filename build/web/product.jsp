<%-- 
    Document   : product
    Created on : Jan 13, 2023, 5:49:55 PM
    Author     : Asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
          <!-- Required meta tags -->
        <meta charset="utf-8">
        <title>EShopper - Bootstrap Shop Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">
        <!-- CSS -->
        <link rel="stylesheet" href="./css/admin.css">

        <!-- Bootstrap CSS v5.2.1 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <jsp:include page="adminNavbar.jsp"></jsp:include>
            <main>
                <div class="container-fluid">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>Manage <b>Products</b></h2>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i
                                                class="material-icons">&#xE147;</i> <span>Add New Product</span></a>                                
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-hover">
                                <thead>

                                    <tr>
                                        <th>
                                            <span class="custom-checkbox">
                                                <input type="checkbox" id="selectAll">
                                                <label for="selectAll"></label>
                                            </span>
                                        </th>
                                        <th>ProductID</th>
                                        <th>Name</th>
                                        <th>CategoryID</th>
                                        <th>Image</th>
                                        <th>Quantity</th>
                                        <th>Description</th>
                                        <th>Price</th>
                                        <th>CreateDate</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test="${requestScope.LIST_PRODUCT == null }">
                                    <c:redirect url="MainController?action=productManager"></c:redirect>
                                </c:if>
                                <c:forEach items="${requestScope.LIST_PRODUCT}" var="p">
                                    <tr>
                                        <td>
                                            <span class="custom-checkbox">
                                                <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                                <label for="checkbox1"></label>
                                            </span>
                                        </td>
                                        <td>${p.productId}</td>
                                        <td>${p.productName}</td>                            
                                        <td>
                                            ${p.categoryId}</td>
                                        <td>  <img src="${p.image}" style="width: 50px; height: 50px" alt=""> </td>
                                        <td>${p.quantity}</td>
                                        <td>${p.proDesc}</td>
                                        <td>${p.price}</td>
                                        <td>${p.createDate}</td>
                                        <c:if test="${p.proStatus == true}">
                                            <td><span style="background-color: green; color: white; padding: 5px; border-radius: 5px">Avaliable</span></td>
                                        </c:if>
                                        <c:if test="${p.proStatus == false}">
                                            <td><span style="background-color: red; color: white; padding: 5px; border-radius: 5px">Disable</span></td>
                                        </c:if>
                                        <td>
                                            <a href="#editProductModal" class="edit" data-toggle="modal"
                                               data-id="${p.productId}"
                                               data-productname="${p.productName}"
                                               data-category="${p.categoryId}"
                                               data-image="${p.image}"
                                               data-quantity="${p.quantity}"
                                               data-createday="${p.createDate}"
                                               data-proprice="${p.price}"
                                               data-status="${p.proStatus}"
                                               data-description="${p.proDesc}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                                            </a>
                                            <a href="#deleteProductModal" class="delete" data-toggle="modal" data-id="${p.productId}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Delete" >&#xE872;</i></a>

                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- Add Modal HTML -->
            <div id="addEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="MainController" method="POST">
                            <div class="modal-header">
                                <h4 class="modal-title">Add Product</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input type="text" class="form-control" name="productName" required>
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control" name="category">
                                        <option value="1" selected>Dresses</option>
                                        <option value="2">Shirts</option>
                                        <option value="3">Jean</option>
                                        <option value="4">Swimwear</option>
                                        <option value="5">Jumpsuts</option>
                                        <option value="6">Blazer</option>
                                        <option value="7">Shoes</option>
                                        <option value="8">Jackets</option>
                                        <option value="9">Bags</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="text" class="form-control" name="img" required>
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input type="number" class="form-control" name="quantity" required min="0">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" type="number" class="form-control" ></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input name="price" step="0.1" min="0" type="number" class="form-control" required>
                                </div>


                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="submit" class="btn btn-success" name="action" value="addProduct">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Modal HTML -->
            <div id="editProductModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="MainController" method="POST">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Product</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input id="productname" type="text" class="form-control" name="productName" required>
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control" name="category">
                                        <option value="1" selected>Dresses</option>
                                        <option value="2">Shirts</option>
                                        <option value="3">Jean</option>
                                        <option value="4">Swimwear</option>
                                        <option value="5">Jumpsuts</option>
                                        <option value="6">Blazer</option>
                                        <option value="7">Shoes</option>
                                        <option value="8">Jackets</option>
                                        <option value="9">Bags</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <input id="product-image" type="text" class="form-control" name="img" required>
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input id="quantity" type="number" class="form-control" name="quantity" required min="0">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea id="description" name="description" type="text" class="form-control" ></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input id="proprice" name="price" step="0.1" min="0" type="number" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Create Day</label>
                                    <input id="createday" name="createDay" class="form-control" required readonly> 
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <option value="0" selected >Disable</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                                <div class="form-group">

                                    <input id="id" name="productId"  class="form-control" hidden="">
                                </div>


                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="submit" class="btn btn-success" name="action" value="edit-product">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Delete Modal HTML -->
            <div id="deleteProductModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="MainController" method="POST">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete Employee</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to delete these Records?</p>
                                <p class="text-warning"><small>This action cannot be undone.</small></p>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="text" id="delete-id"   hidden name="productId">
                                <button type="submit" class="btn btn-danger" value="deleteProduct" name="action" >Delete Product</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </main>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src="./js/admin.js"></script>
    </body>
</html>
