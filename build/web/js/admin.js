$(document).ready(function () {
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Select/Deselect checkboxes
    var checkbox = $('table tbody input[type="checkbox"]');
    $("#selectAll").click(function () {
        if (this.checked) {
            checkbox.each(function () {
                this.checked = true;
            });
        } else {
            checkbox.each(function () {
                this.checked = false;
            });
        }
    });
    checkbox.click(function () {
        if (!this.checked) {
            $("#selectAll").prop("checked", false);
        }
    });
});

$(document).on("click", ".delete", function () {
    var id = $(this).data('id');

    $(".modal-footer #delete-id").val(id);

});

//User

$(document).on("click", ".edit", function () {
    var id = $(this).data('id');
    var firstName = $(this).data('firstname');
    var lastName = $(this).data('lastname');
    var address = $(this).data('address');
    var email = $(this).data('email');
    var password = $(this).data('password');
    var phoneNumber = $(this).data('phone');
    var createDay = $(this).data('createday');


    $(".modal-body #id").val(id);
    $(".modal-body #firstname").val(firstName);
    $(".modal-body #lastname").val(lastName);
    $(".modal-body #address").val(address);
    $(".modal-body #email").val(email);
    $(".modal-body #password").val(password);
    $(".modal-body #phone").val(phoneNumber);
    $(".modal-body #createday").val(createDay);

});

//product


$(document).on("click", ".edit", function () {
    var id = $(this).data('id');
    var name = $(this).data('productname');
    var category = $(this).data('category');
    var img = $(this).data('image');
    var quantity = $(this).data('quantity');
    var description = $(this).data('description');
    var price = $(this).data('proprice');
    var createDay = $(this).data('createday');
    var status = $(this).data('status');



    $(".modal-body #id").val(id);
    $(".modal-body #productname").val(name);
    $(".modal-body #category").val(category);
    $(".modal-body #product-image").val(img);
    $(".modal-body #quantity").val(parseInt(quantity) );
    $(".modal-body #description").val(description);
    $(".modal-body #proprice").val(parseFloat(price));
    $(".modal-body #createday").val(createDay);
    $(".modal-body #status").val(status);


});

