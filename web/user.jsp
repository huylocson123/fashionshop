<%-- 
    Document   : product
    Created on : Jan 13, 2023, 5:49:55 PM
    Author     : Asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <title>EShopper - Bootstrap Shop Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">
        <!-- CSS -->
        <link rel="stylesheet" href="./css/admin.css">

        <!-- Bootstrap CSS v5.2.1 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <jsp:include page="adminNavbar.jsp"></jsp:include>
            <main>
                <div class="container-fluid">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>Manage <b>User</b></h2>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i
                                                class="material-icons">&#xE147;</i> <span>Add New User</span></a>

                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <span class="custom-checkbox">
                                                <input type="checkbox" id="selectAll">
                                                <label for="selectAll"></label>
                                            </span>
                                        </th>
                                        <th>Id</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Address</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Phone Number</th>
                                        <th>CreateDay</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test="${requestScope.LIST_CUSTOMER == null}">
                                    <c:redirect url="MainController?action=loadCustomer"></c:redirect>
                                </c:if>
                                <c:forEach items="${requestScope.LIST_CUSTOMER}" var="c">
                                    <tr>
                                        <td>
                                            <span class="custom-checkbox">
                                                <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                                <label for="checkbox1"></label>
                                            </span>
                                        </td>
                                        <td>${c.customerId}</td>
                                        <td>${c.firstName}</td>
                                        <td>${c.lastName}</td>
                                        <td>${c.address}</td>
                                        <td>${c.email}</td>
                                        <td>${c.password}</td>
                                        <td>${c.phoneNumber}</td>
                                        <td>${c.createDay}</td>
                                        <td>${c.roleId}</td>
                                        <c:if test="${c.status == true}">
                                            <td><span style="background-color: green; color: white; padding: 5px; border-radius: 5px">Active</span></td>
                                        </c:if>
                                        <c:if test="${c.status == false}">
                                            <td><span style="background-color: red; color: white; padding: 5px; border-radius: 5px">Disable</span></td>
                                        </c:if>


                                        <td>
                                            <a href="#editEmployeeModal" class="edit" data-toggle="modal"
                                               data-id="${c.customerId}"
                                               data-firstname="${c.firstName}"
                                               data-lastname="${c.lastName}"
                                               data-address="${c.address}"
                                               data-email="${c.email}"
                                               data-password="${c.password}"
                                               data-phone="${c.phoneNumber}"
                                               data-createday="${c.createDay}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>

                                            <a href="#deleteEmployeeModal" class="delete" data-toggle="modal" data-id="${c.customerId}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Delete" >&#xE872;</i></a>
                                            <div id="deleteEmployeeModal" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form action="MainController" method="POST">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Delete Employee</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Are you sure you want to delete these Records?</p>
                                                                <p class="text-warning"><small>This action cannot be undone.</small></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                                                <input type="hidden" name="delete-id" id="delete-id" value="${c.customerId}" >
                                                                <input type="submit" class="btn btn-danger" name="action" value="deleteUser">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </c:forEach>

                            </tbody>
                        </table>                 
                    </div>
                </div>
            </div>
            <!-- Add Modal HTML -->
            <div id="addEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="MainController" method="POST" accept-charset="UTF-8" >
                            <div class="modal-header">
                                <h4 class="modal-title">Add User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstName" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text"  name="lastName" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input  type="text" name="address" class="form-control" required >
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input  type="email" name="email"  class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input  type="text" name="password"  class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone number</label>
                                    <input type="text" name="phoneNumber" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select name="role" class="form-control">
                                        <option value="1" selected="" >User</option>
                                        <option value="2">Admin</option>

                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="submit" class="btn btn-success" value="addUser" name="action">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Modal HTML -->
            <div id="editEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="MainController" method="POST"  accept-charset="UTF-8">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                  <div class="form-group">
                                    <label>User Id</label>
                                    <input id="id" type="text" name="customerId" class="form-control" readonly>
                                </div>  
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input id="firstname" type="text" name="firstName" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input id="lastname" type="text"  name="lastName"class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input id="address" type="text" name="address" class="form-control" required >
                                </div>                             
                                <div class="form-group">
                                    <label>Email</label>
                                    <input id="email" type="email" name="email"  class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input id="password" type="text" name="password"  class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone number</label>
                                    <input id="phone" type="text" name="phoneNumber" class="form-control" required>
                                </div>     

                                <div class="form-group">
                                    <label>Role</label>
                                    <select name="role" class="form-control">
                                        <option value="1" selected="" >User</option>
                                        <option value="2">Admin</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control">
                                        <option value="0" selected >Disable</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                               
                                 <div class="form-group">
                                    <label>Day Create</label>
                                    <input id="createday" type="text" name="createDay" class="form-control" readonly>
                                </div>  
                            </div>
                            <div class="modal-footer">                             
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <button type="submit" class="btn btn-info" value="edit-user" name="action">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Delete Modal HTML -->

        </main>
        <!-- Bootstrap JavaScript Libraries -->

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <!-- js -->
        <script src="./js/admin.js"></script>
    </body>
</html>
